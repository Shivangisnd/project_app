class Registration < ApplicationRecord
  def process_payment
    customer = Stripe::Customer.create email: email,
                                     card: card_token

    Stripe::Charge.create customer: @user.first_name,
                          amount: @order.subtotal * 100,
                          description: @order.id,
                          currency: 'usd'
  end
end
