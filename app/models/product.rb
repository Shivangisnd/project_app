class Product < ApplicationRecord
    belongs_to :category
    has_many :order_items

    has_attached_file :avatar
    #has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }
  #  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

      validates :prod_name, presence:true
      validates :price,  presence:true, numericality: { greater_than_or_equal_to: 0}
      validates :color,  presence:true
      validates :quantity,  presence:true, numericality: { greater_than: 0}
end
