json.extract! product, :id, :prod_name, :price, :color, :quantity, :created_at, :updated_at
json.url product_url(product, format: :json)
