json.extract! category, :id, :product_id, :content, :created_at, :updated_at
json.url category_url(category, format: :json)
