class OrderItemsController < ApplicationController
  def create
    @order=current_order
    # puts @order.as_json
    # puts "**********"
    @order_items=@order.order_items.new(order_item_params)
    # puts "**********"
    # puts @order_items.as_json
    @order.save!
    @order_items.save!
    # puts @order_items.as_json
    session[:order_id] = @order.id

  end

  def update
    @order=current_order
    puts @order.as_json
    puts "**********"
    @order_items=@order.order_items.find(params[:id])
    puts "**********"
    puts @order_items.as_json
    @order_items.update_attributes(order_item_params)
    flash[:success] = "Product donated!"
    @order_items = @order.order_items
  end

  def destroy
    @order=current_order
    @order_items=@order.order_items.find(params[:id])
    @order_items.destroy
    @order_items = @order.order_items
  end

  private

    def order_item_params
      params.require(:order_item).permit(:quantity,:product_id)
    end
end
