class RegistrationsController < ApplicationController
  def new
    @registration = Registration.new
    @user=current_user
    @order=current_order
  end

  def create
    # @registration = Registration.new(registration_params.merge(email: stripe_params["stripeEmail"],
    #                                                           card_token: stripe_params["stripeToken"]))
    # raise "Please, check charge errors" unless @registration.valid?
    # @registration.process_payment
    # @registration.save!
    # redirect_to @registration, notice: 'Registration was successfully created.'
    # rescue e
    # flash[:error] = e.message
    # render :new
    #
    # @amount = 500
    @user=current_user
    @order=current_order
    @amount = @order.subtotal

    customer = Stripe::Customer.create(
   :email => params[:stripeEmail],
   :source  => params[:stripeToken]
   )

   registration = Registration.create(
     :full_name    => @user.first_name,
     :email =>params[:stripeEmail]
   )

  rescue Stripe::CardError => e
   flash[:error] = e.message
   redirect_to new_registration_path
 end

 private
     def stripe_params
       params.permit :stripeEmail, :stripeToken
     end


end
