class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # before_action :require_no_authentication, :only => [ :new, :create, :cancel ]

  #before_action :require_no_authentication, only: :new

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :allcategories, if: :devise_controller?
  # helper_method :current_user
  helper_method :current_order

  def current_order
    if !session[:order_id].nil?
      @order = Order.find(session[:order_id])
    else
      @order = Order.new
    end
  end

  def hello
   render html: "hello, world!"
  end


  def new_session_path(scope)
   new_user_session_path
  end

  def after_sign_in_path_for(resource)
    if current_user.has_role? :admin
       admindashboards_show_path
    else
       root_path
    end
  end

  protected

  #helper_method :current_user

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name ,:last_name ,:phone_no])
  end

  private
    def allcategories
      @categories=Category.all
    end
  # def current_user
  #   @current_user ||= User.find_by(id: session[:user_id])
  # end


end
