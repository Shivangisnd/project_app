class Users::SessionsController < Devise::SessionsController
  #before_action :allcategories
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
   def new
   end

  # POST /resource/sign_in

   def create
     user = User.from_omniauth(env["omniauth.auth"])
     if user.valid?
       session[:user_id] = user.id
       redirect_to root_path
       #redirect_to request.env['omniauth.origin']
     end
   end


   def destroy
     session[:user_id] = nil
     #sleep 3
     redirect_to root_url
   end

  # DELETE /resource/sign_out


  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
