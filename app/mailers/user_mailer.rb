class UserMailer < ApplicationMailer


  def invoicemail(current_user,categories,products,order,order_items)
    @current_user=current_user
    @categories=Category.all
    @products = Product.all
    @order=order
    #puts @order.inspect
    #puts '-----------'
    @order_items=order_items
    mail to:current_user.email, subject: "Invoice order"
  end
end
