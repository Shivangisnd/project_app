Rails.application.routes.draw do

  get 'order_items/create'

  get 'order_items/update'

  get 'order_items/destroy'

  get 'carts/show'

  get 'productdisplays/home'

  get 'admindashboards/show'

    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #root 'application#hello'
  #devise_for:users
  #devise_for :users, :controllers => { :registrations => "users/registrations",sessions: "users/sessions" }
  #devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks",:registrations => "users/registrations",:sessions=>"users/sessions" }
  root 'dashboard#index'

  get 'products99/:id', to: 'dashboard#list_products',as: :list_products

  get 'product_detail/:id' , to:'dashboard#product_detail' , as: :product_detail

  get 'carts/invoicepage'

  #get 'dashboard/list_products/:id'

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" ,:registrations => "users/registrations"}

  get 'users/auth/facebook', to: 'sessions#create'
  #get "auth/:provider/callback", to: "sessions#create"
  get 'users/auth/github', to: 'sessions#create'
  # get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')

  #get '/auth/:provider/callback', to: 'sessions#create'

  #devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  devise_scope :user do
    get 'sign_up', :to => 'devise/registrations#new'
    get 'sign_in', :to => 'devise/sessions#new'
    get 'users/sign_out', :to => 'devise/sessions#destroy'
  end
  #root 'dashboard#index'

  resources :categories
  resources :products
  #resources :carts, only: [:show]
  resources :carts
  resources :order_items, only: [:create, :update, :destroy]

  get "user_mailer/invoicemail"
  get "carts/createinvoice"

  resources :registrations, only: [:new, :create]

  #get "registrations/new"
  #get "registrations/create"
  #get "registrations/:id", to:'registrations/create' , as: :registrations_detail


  #devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #get  '/signup',  to: 'users#new'
  #get '/signup',  to: 'users#edit'

end
