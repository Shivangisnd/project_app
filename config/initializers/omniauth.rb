OmniAuth.config.logger = Rails.logger

opts = { scope: 'user:email' }

Rails.application.config.middleware.use OmniAuth::Builder do
   provider :facebook, '440237146339639', 'e5a71e5f870556e83d88eeebad91c78d'
   provider :github, Rails.application.secrets.github_client_id, Rails.application.secrets.github_client_secret, opts
end
