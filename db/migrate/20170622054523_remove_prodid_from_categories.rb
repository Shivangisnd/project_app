class RemoveProdidFromCategories < ActiveRecord::Migration[5.1]
  def change
    remove_column :categories, :product_id, :integer
  end
end
