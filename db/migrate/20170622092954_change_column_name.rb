class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :categories, :content, :category_name
  end
end
