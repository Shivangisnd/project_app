class ChangeCategoryColumnType < ActiveRecord::Migration[5.1]
    def up
      change_column :categories, :category_name, :string
    end

    def down
      change_column :categories, :category_name, :text
    end
end
