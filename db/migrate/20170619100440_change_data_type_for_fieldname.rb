class ChangeDataTypeForFieldname < ActiveRecord::Migration[5.1]
  def up
        change_column :users, :phone_no, :integer ,limit:8
  end

    def down
        change_column :users, :phone_no, :integer
    end
end
