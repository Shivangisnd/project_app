class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :prod_name
      t.integer :price
      t.string :color
      t.integer :quantity

      t.timestamps
    end
  end
end
