class CreateRegistrations < ActiveRecord::Migration[5.1]
  def change
    create_table :registrations do |t|
      t.string :full_name
      t.integer :telephone
      t.string :email
      t.string :card_token

      t.timestamps
    end
  end
end
